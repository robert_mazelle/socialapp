package com.robertmazelle.socialapp.entities;


import io.swagger.annotations.ApiModelProperty;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "Social_app_users")
public class User{

    @Id
    private int id;
    protected String username;
    @Column(name = "hash_password")
    private String password;
    protected String firstName;
    protected String surname;
    @OneToMany(mappedBy = "user")
    private List<Post> posts = new ArrayList<>();
    @Column(name = "User_is_online")
    private int online;

    public User() {
    }

    public User(int id, String username, String password) {
        this.id = id;
        this.username = username;
        this.password = password;
    }

    public User(int id, String username, String password, String surname, String firstName) {
        this.username = username;
        this.password = password;
        this.id = id;
        this.surname = surname;
        this.firstName = firstName;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String userName) {
        this.username = userName;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String sureName) {
        this.surname = sureName;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String name) {
        this.firstName = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getOnline() {
        return online;
    }

    public void setOnline(int online) {
        this.online = online;
    }

    public void addPost(Post post) {
        posts.add(post);
    }

    public List<Post> getPosts() {
        return posts;
    }

    public void setPosts(List<Post> posts) {
        this.posts = posts;
    }

    @Override
    public String toString() {
        return "StoredUser{" +
                "id=" + id +
                ", online=" + online +
                ", posts=" + posts +
                ", password='" + password + '\'' +
                ", userName='" + username + '\'' +
                ", firstName='" + firstName + '\'' +
                ", sureName='" + surname + '\'' +
                '}';
    }
}
