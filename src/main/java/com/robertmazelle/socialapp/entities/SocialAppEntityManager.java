package com.robertmazelle.socialapp.entities;



import org.springframework.stereotype.Repository;

import javax.persistence.*;
import java.util.List;

@Repository
public class SocialAppEntityManager {

    EntityManager entityManager;

    SocialAppEntityManager() {
         EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("pu");
         entityManager = entityManagerFactory.createEntityManager();
    }

    public void save(Object o) {
        entityManager.getTransaction().begin();
        entityManager.persist(o);
        entityManager.getTransaction().commit();
    }

    public void remove(Object o) {
        entityManager.getTransaction().begin();
        entityManager.remove(o);
        entityManager.getTransaction().commit();
    }

    public Comment findComment(int id) {
        return entityManager.find(Comment.class, id);
    }

    public Post findPost(int id) {
        return entityManager.find(Post.class, id);
    }

    public User findUser(int id) {
        entityManager.getTransaction().begin();
        User user = entityManager.find(User.class, id);
        entityManager.getTransaction().commit();
        return user;
    }

    public User findUser(String userName) {
        User user;
        try {
            user = entityManager.createQuery("SELECT u FROM User u WHERE u.username LIKE :custName", User.class).setParameter("custName", userName).getSingleResult();
        } catch (NoResultException nre) {
            return null;
        }
        return user;
    }

    public List<User> getAllUsers() {
        List<User> users;
        try {
            Query query = entityManager.createQuery("from User");
            users = query.getResultList();
        } catch (NoResultException nre) {
            return null;
        }
        return users;
    }

}
