package com.robertmazelle.socialapp.entities;

import javax.persistence.*;

@Entity
@Table(name = "Social_App_Comment")
public class Comment {

    @Id
    private int id;
    private String content;
    @Column(name = "comment_date")
    private String date;
    @ManyToOne
    private Post post;
    @ManyToOne
    private User user;

    public Comment(int id, String content, User user, Post post, String date) {
        this.id = id;
        this.content = content;
        this.post = post;
        this.user = user;
        this.date = date;
    }

    public Comment() {
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Post getPost() {
        return post;
    }

    public void setPost(Post post) {
        this.post = post;
    }

    


}
