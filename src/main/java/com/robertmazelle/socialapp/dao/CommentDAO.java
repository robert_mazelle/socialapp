package com.robertmazelle.socialapp.dao;

import com.robertmazelle.socialapp.dto.PostCommentDTO;
import com.robertmazelle.socialapp.entities.Comment;
import com.robertmazelle.socialapp.entities.Post;
import com.robertmazelle.socialapp.entities.SocialAppEntityManager;
import com.robertmazelle.socialapp.entities.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.text.SimpleDateFormat;
import java.util.*;

@Repository
public class CommentDAO {

    private SocialAppEntityManager socialAppEntityManager;

    @Autowired
    public CommentDAO(SocialAppEntityManager socialAppEntityManager) {
        this.socialAppEntityManager = socialAppEntityManager;
    }

    public int newComment(int userId, int postId, String content) {
        Post post = socialAppEntityManager.findPost(postId);
        User user = socialAppEntityManager.findUser(userId);
        int id = findFreeId();
        if (user != null) {
            if (post != null && user.getOnline() == 1) {
                Comment comment = new Comment(id, content, user, post, getDate());
                socialAppEntityManager.save(comment);
                if (socialAppEntityManager.findComment(id).equals(comment)) return 1;
                return -3;
            }
            return -2;
        }
        return -1;
    }


    public int findFreeId() {
        int i = 1;
        while(socialAppEntityManager.findComment(i) != null) i++;
        return i;
    }

   public List<PostCommentDTO> getAllPostComments(int id) {
        Post post = socialAppEntityManager.findPost(id);
        if (post != null) {
            List<Comment> comments = post.getComments();
            List<PostCommentDTO> commentsToShow = new ArrayList<>();
            for (Comment c : comments) {
                commentsToShow.add(new PostCommentDTO(c.getId(), c.getContent(), c.getDate()));
            }
            return  commentsToShow;
        }
        return null;
    }

    public String getDate() {
        Date date = new Date();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd.MM.yyyy HH:mm");
        return simpleDateFormat.format(date);
    }

    public Comment getComment(int id) {
        return socialAppEntityManager.findComment(id);
    }

    public int editComment(String content, int id) {
        Comment comment = socialAppEntityManager.findComment(id);
        if (comment != null && comment.getUser().getOnline() == 1) {
            comment.setContent(content);
            socialAppEntityManager.save(comment);
            if (socialAppEntityManager.findComment(id).equals(comment)) return 1;
            return -2;
        }
        return -1;
    }

    public int deleteComment(int id) {
        Comment comment = socialAppEntityManager.findComment(id);
        if (comment != null && comment.getUser().getOnline() == 1) {
            socialAppEntityManager.remove(comment);
            if (socialAppEntityManager.findComment(id) == null) return 1;
            return -2;
        }
        return -1;
    }
}
