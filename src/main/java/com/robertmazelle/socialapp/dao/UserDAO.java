package com.robertmazelle.socialapp.dao;

import com.robertmazelle.socialapp.entities.SocialAppEntityManager;
import com.robertmazelle.socialapp.entities.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class UserDAO {

    private SocialAppEntityManager socialAppEntityManager;

    @Autowired
    public UserDAO(SocialAppEntityManager socialAppEntityManager) {
        this.socialAppEntityManager = socialAppEntityManager;
    }

    public boolean addOrChangeUser(User user) {
        socialAppEntityManager.save(user);
        if (socialAppEntityManager.findUser(user.getId()).equals(user)) return true;
        return false;
    }

    public int findFreeId() {
        int i = 1;
        while(socialAppEntityManager.findUser(i) != null) i++;
        return i;
    }

    public List<User> getAllUsers() {
        return socialAppEntityManager.getAllUsers();
    }

    public User findUser (int id) {
        return socialAppEntityManager.findUser(id);
    }

    public User findUser (String userName) {
        return socialAppEntityManager.findUser(userName);
    }

}
