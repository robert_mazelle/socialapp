package com.robertmazelle.socialapp.dao;

import com.robertmazelle.socialapp.dto.PostCommentDTO;
import com.robertmazelle.socialapp.entities.Comment;
import com.robertmazelle.socialapp.entities.Post;
import com.robertmazelle.socialapp.entities.SocialAppEntityManager;
import com.robertmazelle.socialapp.entities.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;


import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Repository
public class PostDAO {

    private SocialAppEntityManager socialAppEntityManager;

    @Autowired
    public PostDAO(SocialAppEntityManager socialAppEntityManager) {
        this.socialAppEntityManager = socialAppEntityManager;
    }

    public int newPost(String content, int userId) {
        User user = socialAppEntityManager.findUser(userId);
        if (user != null && user.getOnline() == 1) {
            int id = findFreeId();
            Post post = new Post(id, content, user, getDate());
            socialAppEntityManager.save(post);
            if (socialAppEntityManager.findPost(id).equals(post)) return 1;
            else return -2;
        }
        return -1;
    }

    public int findFreeId() {
        int i = 1;
        while(socialAppEntityManager.findPost(i) != null) i++;
        return i;
    }

    public List<PostCommentDTO> getPosts(User user) {
       List<PostCommentDTO> postsToShow = new ArrayList<>();
       List<Post> userPosts = user.getPosts();
        for (Post p : userPosts) {
            if (p.getUser().equals(user)) postsToShow.add(new PostCommentDTO(p.getUser().getId(), p.getContent(), p.getDate()));
        }
        return postsToShow;
    }

    public int editPost(String content, int id) {
        Post post = socialAppEntityManager.findPost(id);
        if (post != null) {
            post.setContent(content);
            socialAppEntityManager.save(post);
            if (socialAppEntityManager.findPost(id).equals(post)) return 1;
            else return -2;
        }
        return -1;
    }

    public int remove(int id) {
        Post post = socialAppEntityManager.findPost(id);
        if (post != null && post.getUser().getOnline() == 1) {
            for(Comment c : post.getComments()) socialAppEntityManager.remove(c);
            socialAppEntityManager.remove(post);
            if (socialAppEntityManager.findPost(id) == null) return 1;
            return -2;
        }
        return -1;
    }

    public Post findPost(int id) {
        return socialAppEntityManager.findPost(id);
    }

    public String getDate() {
        Date date = new Date();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd.MM.yyyy HH:mm");
        return simpleDateFormat.format(date);
    }

}
