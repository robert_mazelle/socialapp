package com.robertmazelle.socialapp.service;

import com.robertmazelle.socialapp.dao.PostDAO;
import com.robertmazelle.socialapp.dao.UserDAO;
import com.robertmazelle.socialapp.dto.ChangeName;
import com.robertmazelle.socialapp.dto.ConsumedUser;
import com.robertmazelle.socialapp.dto.PasswordChange;
import com.robertmazelle.socialapp.dto.ShownUser;
import com.robertmazelle.socialapp.entities.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;


import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;

@Service
public class UserService {

    private UserDAO userDAO;
    private PostDAO postDAO;

    @Autowired
    public UserService(UserDAO userDAO, PostDAO postDAO) {
        this.userDAO = userDAO;
        this.postDAO = postDAO;
    }

    public ResponseEntity<String> addUser(ConsumedUser user) {
        if (userDAO.findUser(user.getUserName()) == null) {
            if (userDAO.addOrChangeUser(changeUserType(user))) return new ResponseEntity<>("User successfully added", HttpStatus.CREATED);
            return new ResponseEntity<>("Something went wrong, try again", HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>("There is already a user with this username.", HttpStatus.BAD_REQUEST);
    }

    public List<ShownUser> getAllUsers() {
        List<User> usersGot = new ArrayList<>(userDAO.getAllUsers());
        List<ShownUser> usersToShow = new ArrayList<>();
        for (User u : usersGot) {
            usersToShow.add(new ShownUser(u.getUsername(),u.getFirstName(),u.getSurname(), postDAO.getPosts(u)));
        }
        return usersToShow;
    }

    public User changeUserType(ConsumedUser user) {
        return new User(userDAO.findFreeId(), user.getUserName(), hashUserPassword(user.getPassword()));
    }

    public String hashUserPassword(String password) {

        StringBuilder hexString = new StringBuilder();

        try {
            MessageDigest digest = MessageDigest.getInstance("SHA-256");
            digest.reset();
            digest.update(password.getBytes(StandardCharsets.UTF_8));
            hexString = new StringBuilder(String.format("%040x", new BigInteger(1, digest.digest())));
        } catch (NoSuchAlgorithmException nsae) {

        }

        while (hexString.length() < 64) {
            hexString.insert(0, '0');
        }

        return hexString.toString();
    }

    public ResponseEntity<String> login(ConsumedUser user) {
        User userStored = userDAO.findUser(user.getUserName());
        if (userStored != null && userStored.getOnline() != 1) {
            String consumedPassword = hashUserPassword(user.getPassword());
            if (consumedPassword.equals(userStored.getPassword())) {
                userStored.setOnline(1);
                if (userDAO.addOrChangeUser(userStored)) return new ResponseEntity<>("User successfully logged in.", HttpStatus.OK);
                else return new ResponseEntity<>("Something went wrong, try again later.", HttpStatus.INTERNAL_SERVER_ERROR);
            }
            return new ResponseEntity<>("Wrong username or password.", HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>("Wrong username or password.", HttpStatus.BAD_REQUEST);
    }

    public ResponseEntity<String> changeName(int id, ChangeName changeName) {
        User storedUser = userDAO.findUser(id);
        if (storedUser != null && storedUser.getOnline() == 1) {
            storedUser.setFirstName(changeName.getFirstName());
            storedUser.setSurname(changeName.getSurname());
            if (userDAO.addOrChangeUser(storedUser)) return new ResponseEntity<>("User successfully changed.", HttpStatus.OK);
            else return new ResponseEntity<>("Something went wrong, try again later.", HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>("Wrong user ID", HttpStatus.BAD_REQUEST);
    }

    public ResponseEntity<String> logout(int id) {
        User user = userDAO.findUser(id);
        if (user != null && user.getOnline() == 1) {
            user.setOnline(0);
            if (userDAO.addOrChangeUser(user)) return new ResponseEntity<>("User successfully logged out.", HttpStatus.OK);
            else return new ResponseEntity<>("Something went wrong, try again later.", HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>("There is no user with this ID.", HttpStatus.BAD_REQUEST);
    }

    public ResponseEntity<String> changePassword(PasswordChange password, int id) {
        User user = userDAO.findUser(id);
        if (user != null && user.getOnline() == 1) {
            String consumedPassword = hashUserPassword(password.getOldPassword());
            if (consumedPassword.equals(user.getPassword())) {
                if (password.getNewPassword().equals(password.getConfirmPassword())) {
                    user.setPassword(hashUserPassword(password.getNewPassword()));
                    if (userDAO.addOrChangeUser(user))
                        return new ResponseEntity<>("Password successfully changed.", HttpStatus.OK);
                    else
                        return new ResponseEntity<>("Something went wrong, try again later.", HttpStatus.INTERNAL_SERVER_ERROR);
                }
                return new ResponseEntity<>("The new passwords don't match.", HttpStatus.BAD_REQUEST);
            }
            return new ResponseEntity<>("The old password is wrong.", HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>("There is no user with this ID.", HttpStatus.BAD_REQUEST);
    }
}
