package com.robertmazelle.socialapp.service;

import com.robertmazelle.socialapp.dao.CommentDAO;
import com.robertmazelle.socialapp.dao.PostDAO;
import com.robertmazelle.socialapp.dao.UserDAO;
import com.robertmazelle.socialapp.dto.PostCommentDTO;
import com.robertmazelle.socialapp.entities.Comment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CommentService {

    private CommentDAO commentDAO;


    @Autowired
    public CommentService(CommentDAO commentDAO) {
        this.commentDAO = commentDAO;

    }

    public ResponseEntity<String> newComment(int userId, int postID, String comment) {
        int response = commentDAO.newComment(userId, postID, comment);
        switch (response) {
            case -3 : return new ResponseEntity<>("Something went wrong. Try again later.", HttpStatus.INTERNAL_SERVER_ERROR);
            case -2 : return new ResponseEntity<>("There is no post with such ID.", HttpStatus.BAD_REQUEST);
            case -1 : return new ResponseEntity<>("There is no user with such ID.", HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>("Comment successfully added", HttpStatus.CREATED);
    }

    public List<PostCommentDTO> getAllPostComments(int id) {
        return commentDAO.getAllPostComments(id);
    }

    public ResponseEntity<String> deleteComment(int id) {
       int response = commentDAO.deleteComment(id);
       if (response == -1) return new ResponseEntity<>("There is no comment with this ID", HttpStatus.BAD_REQUEST);
       else if (response == -2) return new ResponseEntity<>("Something went wrong. Try again later.", HttpStatus.INTERNAL_SERVER_ERROR);
       return new ResponseEntity<>("Comment successfully deleted.", HttpStatus.OK);
    }

    public ResponseEntity<String> editComment(int id, String newContent) {
        int response = commentDAO.editComment(newContent, id);
        if (response == -2) return new ResponseEntity<>("Something went wrong. Try again later", HttpStatus.INTERNAL_SERVER_ERROR);
        else if (response == -1) return new ResponseEntity<>("There is no comment with this ID", HttpStatus.BAD_REQUEST);
        return new ResponseEntity<>("Comment successfully edited.", HttpStatus.OK);
    }
}
