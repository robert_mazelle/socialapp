package com.robertmazelle.socialapp.service;

import com.robertmazelle.socialapp.dao.PostDAO;
import com.robertmazelle.socialapp.dao.UserDAO;
import com.robertmazelle.socialapp.dto.PostCommentDTO;
import com.robertmazelle.socialapp.entities.Post;
import com.robertmazelle.socialapp.entities.SocialAppEntityManager;
import com.robertmazelle.socialapp.entities.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class PostService {

    private PostDAO postDAO;
    private UserDAO userDAO;

    @Autowired
    public PostService(PostDAO postDAO, UserDAO userDAO, SocialAppEntityManager socialAppEntityManager) {
        this.postDAO = postDAO;
        this.userDAO = userDAO;
    }

    public ResponseEntity<String> newPost(int id, String content) {
        int response = postDAO.newPost(content, id);
        if (response == -1) return new ResponseEntity<>("There is no such user.", HttpStatus.BAD_REQUEST);
        else if (response == -2) return new ResponseEntity<>("Something went wrong, try again later", HttpStatus.INTERNAL_SERVER_ERROR);
        return new ResponseEntity<>("Post successfully added.", HttpStatus.CREATED);
    }

    public List<PostCommentDTO> getAllUserPosts(int id) {
        return postDAO.getPosts(userDAO.findUser(id));
    }

    public ResponseEntity<String> editPost(String content, int id) {
        int response = postDAO.editPost(content, id);
        if (response == -1) return new ResponseEntity<>("There is no post with such ID.", HttpStatus.BAD_REQUEST);
        else if (response == -2) return new ResponseEntity<>("Something went wrong, try again later", HttpStatus.INTERNAL_SERVER_ERROR);
        return new ResponseEntity<>("Post successfully modified.", HttpStatus.OK);
    }

    public ResponseEntity<String> deletePost(int id) {
        int response = postDAO.remove(id);
        if (response == -1) return new ResponseEntity<>("There is no post with such ID.", HttpStatus.BAD_REQUEST);
        else if (response == -2) return new ResponseEntity<>("Something went wrong, try again later", HttpStatus.INTERNAL_SERVER_ERROR);
        return new ResponseEntity<>("Post successfully deleted.", HttpStatus.OK);
    }


}
