package com.robertmazelle.socialapp.dto;


import io.swagger.annotations.ApiModelProperty;

public class ConsumedUser extends ShownUser {

    @ApiModelProperty(example = "1111mm", notes = "The password to access the social app by a single user.", required = true)
    protected String password;


    public ConsumedUser(String userName, String password) {
        this.userName = userName;
        this.password = password;
    }

    public ConsumedUser() {
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

}
