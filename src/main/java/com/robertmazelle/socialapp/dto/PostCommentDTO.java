package com.robertmazelle.socialapp.dto;

import io.swagger.annotations.ApiModelProperty;

public class PostCommentDTO {

    @ApiModelProperty(example = "5", notes = "The id of the user who wrote the post or comment.")
    private int userId;
    @ApiModelProperty(example = "I know nothing.", notes = "The text of the post or comment.")
    private String content;
    @ApiModelProperty(example = "08.08.2008 08:08", notes = "The date of posting the original post or comment.")
    private String date;

    public PostCommentDTO(int userId, String content, String date) {
        this.userId = userId;
        this.content = content;
        this.date = date;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    @Override
    public String toString() {
        return "Post{" +
                ", content='" + content + '\'' +
                '}';
    }
}
