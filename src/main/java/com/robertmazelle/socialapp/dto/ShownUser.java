package com.robertmazelle.socialapp.dto;

import io.swagger.annotations.ApiModelProperty;

import java.util.List;

public class ShownUser extends ChangeName {

    @ApiModelProperty(example = "adam_smith", notes = "The username of a specific user.", required = true)
    protected String userName;

    public ShownUser(String userName, String firstName, String sureName, List<PostCommentDTO> posts) {
        this.userName = userName;
        this.firstName = firstName;
        this.surname = sureName;
    }

    public ShownUser() {
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    @Override
    public String toString() {
        return "ShownUser{" +
                "userName='" + userName + '\'' +
                ", firstName='" + firstName + '\'' +
                ", sureName='" + surname + '\'' +
                '}';
    }
}
