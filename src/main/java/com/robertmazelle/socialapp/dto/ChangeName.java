package com.robertmazelle.socialapp.dto;

import io.swagger.annotations.ApiModelProperty;

public class ChangeName {

    @ApiModelProperty(example = "Adam", notes = "The first name of the user.")
    protected String firstName;
    @ApiModelProperty(example = "Smith", notes = "The surname of the user.")
    protected String surname;

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }
}
