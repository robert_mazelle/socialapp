package com.robertmazelle.socialapp.dto;

import io.swagger.annotations.ApiModelProperty;

public class PasswordChange {
    @ApiModelProperty(example = "1234b", notes = "The old password, that the user wants to change.")
    private String oldPassword;
    @ApiModelProperty(example = "new1234", notes = "The new password")
    private String newPassword;
    @ApiModelProperty(example = "new1234", notes = "Again the new password, to minimize the risk of a mistake.")
    private String confirmPassword;

    public String getOldPassword() {
        return oldPassword;
    }

    public String getNewPassword() {
        return newPassword;
    }

    public String getConfirmPassword() {
        return confirmPassword;
    }

}
