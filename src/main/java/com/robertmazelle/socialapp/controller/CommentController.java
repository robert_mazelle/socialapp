package com.robertmazelle.socialapp.controller;


import com.robertmazelle.socialapp.dto.PostCommentDTO;
import com.robertmazelle.socialapp.service.CommentService;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("comments")
public class CommentController {

    private CommentService commentService;

    @Autowired
    public CommentController(CommentService commentService) {
        this.commentService = commentService;
    }

    @ApiOperation(value = "Adding a new comment to a post.")
    @PostMapping(value = "{userId}/{postId}/new-comment")
    public ResponseEntity<String> newComment(
            @ApiParam("ID of the user who wants to add a comment") @PathVariable int userId,
            @ApiParam("ID of the post to comment") @PathVariable int postId,
            @ApiParam(value = "The text of the comment") @RequestParam(defaultValue = "I like bananas.") String comment) {
        return commentService.newComment(userId, postId, comment);
    }

    @ApiOperation(value = "Getting all the comments of a single post.")
    @GetMapping(value = "{postId}/get-comments")
    public List<PostCommentDTO> getAllPostComments(@ApiParam("The ID of the post to get all its comments.") @PathVariable int postId) {
        return commentService.getAllPostComments(postId);
    }

    @ApiOperation(value = "Deleting a single comment.")
    @DeleteMapping(value = "{id}")
    public ResponseEntity<String> deleteComment(@ApiParam("The ID of the comment to delete") @PathVariable int id) {
        return commentService.deleteComment(id);
    }

    @ApiOperation(value = "Changing the content of a single comment.")
    @PutMapping(value = "{id}")
    public ResponseEntity<String> editComment(@ApiParam("The ID of the comment to edit") @PathVariable int id, @ApiParam(value = "New comment text.") @RequestParam(defaultValue = "I like apples.") String newContent) {
        return commentService.editComment(id, newContent + " (Edited)");
    }
}
