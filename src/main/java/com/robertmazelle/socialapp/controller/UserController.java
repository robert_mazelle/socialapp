package com.robertmazelle.socialapp.controller;

import com.robertmazelle.socialapp.dto.ChangeName;
import com.robertmazelle.socialapp.dto.ConsumedUser;
import com.robertmazelle.socialapp.dto.PasswordChange;
import com.robertmazelle.socialapp.dto.ShownUser;
import com.robertmazelle.socialapp.service.UserService;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/users")
public class UserController {

    UserService userService;

    @Autowired
    public UserController(UserService socialAppService) {
        this.userService = socialAppService;
    }

    @ApiOperation(value = "Adding a new user. The username has to be unique.")
    @PostMapping(value = "new-user")
    public ResponseEntity<String> newUser(@ApiParam("Parameters of the new user") @RequestBody ConsumedUser user) {
        return userService.addUser(user);
    }

    @ApiOperation(value = "Logging the user in. User needs to be logged out.")
    @PutMapping(value = "login")
    public ResponseEntity<String> login(@ApiParam("Parameters of the user, that wants to log in.") @RequestBody ConsumedUser user) {
        return userService.login(user);
    }

    @ApiOperation(value = "Logging the user out. User needs to be logged in")
    @PutMapping(value = "logout/{id}")
    public ResponseEntity<String> logout(@ApiParam("User ID to logout") @PathVariable int id) {
        return userService.logout(id);
    }

    @ApiOperation(value = "Changing the first name and surname of the user. User needs to be logged in.")
    @PutMapping(value = "{id}/change-name")
    public ResponseEntity<String> changeName(@ApiParam("User ID to logout") @PathVariable int id, @ApiParam("Parameters of the user, who wants to change his personal data.") @RequestBody ChangeName changeName) {
        return userService.changeName(id, changeName);
    }

    @ApiOperation(value = "Changing the password of the user. User needs to be logged in.")
    @PutMapping(value = "{id}/change-password")
    public ResponseEntity<String> changePassword(@ApiParam("User ID who wants to change his password") @PathVariable int id, @ApiParam("Old and new password data.") @RequestBody PasswordChange password) {
        return userService.changePassword(password, id);
    }

    @ApiOperation(value = "Showing the list of the registered users.")
    @GetMapping
    public List<ShownUser> getAllUsers() {
        return userService.getAllUsers();
    }

}


