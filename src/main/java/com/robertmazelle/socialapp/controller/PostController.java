package com.robertmazelle.socialapp.controller;


import com.robertmazelle.socialapp.dto.PostCommentDTO;
import com.robertmazelle.socialapp.service.PostService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/posts")
public class PostController {

    private PostService postService;

    @Autowired
    public PostController(PostService postService) {
        this.postService = postService;
    }

    @ApiOperation(value = "Adding a single post by a user. The user must be online.")
    @PostMapping(value = "{userId}/new-post")
    public ResponseEntity<String> newPost(@ApiParam("User ID who wants to post something.") @PathVariable int userId, @ApiParam(value = "The text of the post.") @RequestParam(defaultValue = "I just got engaged!.") String content) {
        return postService.newPost(userId, content);
    }

    @ApiOperation(value = "Showing all posts of a single user.")
    @GetMapping(value = "{id}")
    public List<PostCommentDTO> getAllUserPosts(@ApiParam("User ID to show all his posts") @PathVariable int id) {
        return postService.getAllUserPosts(id);
    }

    @ApiOperation(value = "Modifying a single post. The owner of the post must be logged in.")
    @PutMapping(value = "{id}/modify")
    public ResponseEntity<String> editPost(@ApiParam("Id of the post to modify.") @PathVariable int id, @ApiParam(value = "The new content of the post.") @RequestParam(defaultValue = "I like fast cars.") String content) {
        return postService.editPost(content, id);
    }

    @ApiOperation(value = "Deleting a single post. The owner of the post must be online.")
    @DeleteMapping(value = "{id}")
    public ResponseEntity<String> deletePost(@ApiParam("The ID of the post to delete") @PathVariable int id) {
        return postService.deletePost(id);
    }

}
